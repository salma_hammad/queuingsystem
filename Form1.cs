﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Algorithm__Queuing_System_
{
    public partial class Form1 : Form
    {
        int NumOfTasks;
        Random rand_ServiceTime = new Random();


        #region Service Time Variables
        List<float> ServiceTimes;
        List<float> Probabilities_ServiceTime;
        List<float> CumulativeProbability_ServiceTime;
        List<float> RandomNumbers_ServiceTime;
        List<float> Calculated_ServiceTime;
         #endregion
        #region Inter Arrival Time Variables
        List<float> InterArrival;
        List<float> Probabilities_InterArrivalTime;
        List<float> CumulativeProbability_InterArrivalTime;
        List<float> RandomNumbers_InterArrivalTime;
        List<float> Calculated_InterArrivalTime;
         #endregion 

        List<Task> Tasks = new List<Task>();
        float LastArrivalTime = 0;
        float ServerIdleTime = 0;
        float LastEndServiceTime = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            ServiceTimes = new List<float>();
            Probabilities_ServiceTime = new List<float>();
            CumulativeProbability_ServiceTime = new List<float>();
            RandomNumbers_ServiceTime = new List<float>();
            Calculated_ServiceTime = new List<float>();

             InterArrival = new List<float>();
             Probabilities_InterArrivalTime = new List<float>();
             CumulativeProbability_InterArrivalTime = new List<float>();
             RandomNumbers_InterArrivalTime = new List<float>();
             Calculated_InterArrivalTime = new List<float>();

            NumOfTasks = int.Parse(txt_NoOfTasks.Text);
            //calculate service time
            for(int i=0 ; i<dataGridView1.RowCount-1; i++)
            {
                string ValueOfColumn1 = dataGridView1.Rows[i].Cells[0].Value.ToString();
                string ValueOfColumn2 = dataGridView1.Rows[i].Cells[1].Value.ToString();
                ServiceTimes.Add(float.Parse(ValueOfColumn1));
                Probabilities_ServiceTime.Add(float.Parse(ValueOfColumn2));
            }
            Cumulative_Probability();
            GetRandomNumber();
            Calculate_ServiceTime();

            //Calculate interarrival time
            for (int i = 0; i < dataGridView2.RowCount-1; i++)
            {
                string ValueOfColumn1 = dataGridView2.Rows[i].Cells[0].Value.ToString();
                string ValueOfColumn2 = dataGridView2.Rows[i].Cells[1].Value.ToString();
                InterArrival.Add(float.Parse(ValueOfColumn1));
                Probabilities_InterArrivalTime.Add(float.Parse(ValueOfColumn2));
            }
            Cumulative_Probability_InterArrival();
            GetRandomNumber_InterArrival();
            Calculate_InterArrivalTime();

            run();
            view();
        }

         
        //Functions Of ServiceTime Table
        #region Service Time Functions
        private void Cumulative_Probability()
        {
            for(int i=0 ; i<Probabilities_ServiceTime.Count ; i++)
            {
                if (i == 0)
                {
                    CumulativeProbability_ServiceTime.Add(Probabilities_ServiceTime[i]);
                }
                else
                {
                    CumulativeProbability_ServiceTime.Add(Probabilities_ServiceTime[i] + CumulativeProbability_ServiceTime[i - 1]);
                }
            }
        }

        private void GetRandomNumber()
        {
            for(int i=0 ; i<NumOfTasks ; i++)
            {
                RandomNumbers_ServiceTime.Add(float.Parse(rand_ServiceTime.NextDouble().ToString()));
            }
        }

        private void Calculate_ServiceTime()
        {
            for(int i=0 ; i<RandomNumbers_ServiceTime.Count ; i++)
            {
                for(int j=0 ; j<CumulativeProbability_ServiceTime.Count ; j++)
                {
                    if(RandomNumbers_ServiceTime[i] < CumulativeProbability_ServiceTime[j])
                    {
                        Calculated_ServiceTime.Add(ServiceTimes[j]);
                        break;
                    }
                }
            }
        }

        #endregion 



        //Functions Of Inter ArrivalTime Table
        #region InterArrival Time Functions
        private void Cumulative_Probability_InterArrival()
        {
            for (int i = 0; i < Probabilities_InterArrivalTime.Count; i++)
            {
                if (i == 0)
                {
                    CumulativeProbability_InterArrivalTime.Add(Probabilities_InterArrivalTime[i]);
                }
                else
                {
                    CumulativeProbability_InterArrivalTime.Add(Probabilities_InterArrivalTime[i] + CumulativeProbability_InterArrivalTime[i - 1]);
                }
            }
        }

        private void GetRandomNumber_InterArrival()
        {
            for (int i = 0; i < NumOfTasks; i++)
            {
                RandomNumbers_InterArrivalTime.Add(float.Parse(rand_ServiceTime.NextDouble().ToString()));
            }
        }

        private void Calculate_InterArrivalTime()
        {
            for (int i = 0; i < RandomNumbers_InterArrivalTime.Count; i++)
            {
                for (int j = 0; j < CumulativeProbability_InterArrivalTime.Count; j++)
                {
                    if (RandomNumbers_InterArrivalTime[i] < CumulativeProbability_InterArrivalTime[j])
                    {
                        Calculated_InterArrivalTime.Add(InterArrival[j]);
                        break;
                    }
                }
            }
        }

        #endregion 


        private void run()
        {
            for(int i=0 ; i<NumOfTasks ; i++)
            {
                Task t = new Task();
                t.TaskNumber = i + 1;
                if (i == 0)
                {
                    t.InterArrivalTime = t.ArrivalTime = LastArrivalTime = t.ServiceBeginTime = 0;
                    t.ServiceTime = Calculated_ServiceTime[i];
                    t.ServiceEndTime = t.ServiceTime + t.ServiceBeginTime;
                    LastEndServiceTime = t.ServiceEndTime;

                }
                else
                {
                    t.InterArrivalTime = Calculated_InterArrivalTime[i - 1];
                    t.ArrivalTime = t.InterArrivalTime + LastArrivalTime;
                    LastArrivalTime = t.ArrivalTime;
                    t.ServiceTime = Calculated_ServiceTime[i];
                    
                    if(LastEndServiceTime ==  t.ArrivalTime)
                    {
                        t.ServiceBeginTime = t.ArrivalTime;
                        t.ServiceEndTime = t.ServiceBeginTime + t.ServiceTime;
                        LastEndServiceTime = t.ServiceEndTime;
                    }
                    else if(LastEndServiceTime < t.ArrivalTime)
                    {
                        t.ServiceBeginTime = t.ArrivalTime;
                        ServerIdleTime += (t.ArrivalTime - LastEndServiceTime);
                        t.ServiceEndTime = t.ServiceBeginTime + t.ServiceTime;
                        LastEndServiceTime = t.ServiceEndTime;
                    }
                    else if(LastEndServiceTime > t.ArrivalTime)
                    {
                        t.ServiceBeginTime = LastEndServiceTime;
                        t.waitingtime = LastEndServiceTime - t.ArrivalTime;
                        t.ServiceEndTime = t.ServiceBeginTime + t.ServiceTime;
                        LastEndServiceTime = t.ServiceEndTime;
                    }
                }
                Tasks.Add(t);
           
            }
        }

        private void view()
        {
            for(int i=0 ; i<NumOfTasks ; i++)
            {
                dataGridView3.Rows.Add();
                dataGridView3.Rows[i].Cells[0].Value = Tasks[i].TaskNumber;
                dataGridView3.Rows[i].Cells[1].Value = Tasks[i].InterArrivalTime;
                dataGridView3.Rows[i].Cells[2].Value = Tasks[i].ArrivalTime;
                dataGridView3.Rows[i].Cells[3].Value = Tasks[i].ServiceBeginTime;
                dataGridView3.Rows[i].Cells[4].Value = Tasks[i].ServiceTime;
                dataGridView3.Rows[i].Cells[5].Value = Tasks[i].ServiceEndTime;
                dataGridView3.Rows[i].Cells[6].Value = Tasks[i].waitingtime;

            }

            txt_serverIdleTime.Text = ServerIdleTime.ToString();
        }

    
    }
}
